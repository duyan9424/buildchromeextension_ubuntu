# Build cross-browser extension in Ubuntu 16.04 #
* Use the [Kango framework](http://kangoextensions.com) - cross-browser extension framework

### Preparation of environment to work with Kango. [Ubuntu 16.04] ###
Before you begin working with Kango you should make a few steps:

1. Install [Python 2.7](http://www.python.org/download/) 
    ```
    $ python --version
    Python 2.7.12
    ```
2. Download Download Kango v1.8.0
    ```
    $ wget http://kangoextensions.com/kango/kango-framework-latest.zip
    $ sudo apt-get -y install unzip
    $ unzip kango-framework-latest.zip -d kango-framework
    ```
### Creating new Kango sample project ###
1. List Files
    ```
    $ ls
    kango-framework/
    kango-framework-latest.zip
    ```
2. Run `sh createKangoProject.sh` or Run kango.py with create command:
    ```
    $ python ./kango-framework/kango.py create "TestExtension"
    [INFO] Contact extensions@kangoextensions.com to enable IE support
    [INFO] Running Kango v1.8.0
    $ Input project name: TestExtension
    [INFO] Creating project...
    [INFO] Project created in directory /home/ubuntu/marketing_helper_extension/TestExtension
    ```
3. List Files
    ```
    .
    ├── certificates
    │   └── chrome.pem
    └── src
        ├── chrome
        │   └── extension_info.json
        ├── common
        │   ├── extension_info.json
        │   ├── icons
        │   │   ├── button.png
        │   │   ├── icon100.png
        │   │   ├── icon128.png
        │   │   ├── icon32.png
        │   │   └── icon48.png
        │   └── main.js
        ├── firefox
        │   └── extension_info.json
        ├── ie
        │   └── extension_info.json
        └── safari
            └── extension_info.json
    ```
4. Building extension. Run `sh buildingExtension.sh` or Run kango.py with build command:
    ```
    $ python kango-framework/kango.py build "TestExtension"
    [INFO] Contact extensions@kangoextensions.com to enable IE support
    [INFO] Running Kango v1.8.0
    [INFO] Building chrome extension...
    [INFO] Chrome/Chromium is not installed, trying OpenSSL...
    [INFO] Building firefox extension...
    [INFO] Building safari extension...
    ```
5. List Files
    ```
    .
    ├── output
       ├── chrome
       ├── firefox
       ├── safari
       ├── testextension_0.9.0_chrome_webstore.zip
       ├── testextension_0.9.0.crx
       └── testextension_0.9.0.xpi
    ```
6. How to install a Chrome extension:
    1. Click Preferences > Go to More tools > then Extensions > check Develper Mode activate.
    2. For User:
        * Download a .CRX file in `output/testextension_0.9.0.crx` project directory.
        * then drag and drop the .CRX file on the extension page.
    3. For Developer for testing:
        * Load unpacked extension.
        * Select extension directory: `/output/chrome`